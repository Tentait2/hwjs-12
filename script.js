// 1. Почему для работы с input не рекомендуется использовать события клавиатуры?
// Потому что для работы с инпутом могут так же использоваться голосовые наборы и тд, а события клавиатуры к этому не подходят.

let btn = document.querySelectorAll('button')

document.addEventListener('keydown', (el) =>{
    btn.forEach((btnElem) =>{
        btnElem.style.background = 'black'
        if(el.key === btnElem.textContent || el.key.toUpperCase() === btnElem.textContent){
            btnElem.style.background = "blue"
            console.log(btnElem.textContent)
        }
    })
})

